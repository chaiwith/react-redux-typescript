import React, { FC } from 'react';
import { useHistory } from 'react-router-dom';

const About: FC = () => {
	const history = useHistory();
	return (
		<>
			<h1>About</h1>
			<br />
			<button onClick={history.goBack}>Back</button>
		</>
	);
};

export default About;
