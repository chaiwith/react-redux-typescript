import React, { FC } from "react";
import { Link} from "react-router-dom";


const Home: FC = () => {
    return (
        <>
            <h1>Home</h1>
            <Link to="/about">About</Link>
            <br/>
            <Link to="/todo">Todo</Link>
        </>
    );
};

export default Home;

