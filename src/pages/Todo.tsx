import React,{ FC } from "react";
import TodoTable from "../components/TodoTable"
import CreateTodo from "../components/CreateTodo"
import { useHistory} from "react-router-dom";

const Todo: FC = () => {
    const history = useHistory();
    return (
        <>
            <button onClick={history.goBack}>Back</button>
            <br/>
            <CreateTodo />
            <TodoTable />
        </>
    );
};

export default Todo;

