import axios from "axios";


export const getTodoList = async () => {
try {
    const response = await axios({
        url:'http://localhost:8888/todo-list',
        method:"GET",
        timeout: 30000,
    });

    return response !== undefined && response.data && response.data.code
      ? response.data
      : null;
  } catch (error) {
    return null;
  }
};