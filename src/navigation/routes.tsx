const DefaultRoutes = {
    Home: {
        path: "/"
    },
    Todo: {
        path: "/Todo"
    },
    About: {
        path:"/About"
    }
}

export default DefaultRoutes;