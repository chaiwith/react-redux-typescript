import React from "react";
import {  Route ,Switch,BrowserRouter} from "react-router-dom";
import DefaultRoutes from "../navigation/routes";
import Todo from "../pages/Todo"
import Home from "../pages/Home"
import About from "../pages/About"

function Router() {
  return (
    <BrowserRouter>
			<Switch>
      <Route
        exact={true}
        component={Home}
        path={DefaultRoutes.Home.path}
        />
        <Route
        exact={true}
        component={About}
        path={DefaultRoutes.About.path}
      />
        <Route
        exact={true}
        component={Todo}
        path={DefaultRoutes.Todo.path}
      />
			</Switch>
</BrowserRouter>
  );
}

export default Router;
