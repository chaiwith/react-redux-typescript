import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getTodoState } from "../store/todo/selectors";
import { checkTodo, deleteTodo } from "../store/todo/actions";
import { getTodoList } from "../service/todo"
import { addTodo } from "../store/todo/actions";

const TodoTable = () => {
	const todos = useSelector(getTodoState);
	const dispatch = useDispatch();
	
	useEffect(() => {
    	getTodo();
	}, []);
	
	const getTodo = useCallback(async () => {
      const result = await getTodoList();
      if (!result) {
        return;
	  }
    	setTodo(result.todoList)
	}, []);
	
	const setTodo = useCallback((todolist: any) => {
		const todo = todolist
		todo.map((item: any) => {
			dispatch(addTodo({ todo: item.todo, isCheck: item.isCheck }));
      	});
	},[])

	const handleCheck = (event: React.ChangeEvent<HTMLInputElement>) => {
		const index = +event.target.value;
		dispatch(checkTodo(index));
	};

	const handleClick = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		const index = +event.currentTarget.value;
		dispatch(deleteTodo(index));
	};
	return (
		<div className="container">
			<table className="table is-fullwidth is-hoverable">
				<thead>
					<tr>
						<th>#</th>
						<th>Todo</th>

						<th>Check</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{todos.map((todo, key) => {
						return (
							<tr key={key}>
								<td>{key + 1}</td>
								<td>{todo.isCheck ? <del> {todo.todo} </del> : todo.todo}</td>
								<td>
									<p>{todo.isCheck}</p>
									<label className="checkbox">
										<input
											type="checkbox"
											checked={todo.isCheck}
											onChange={handleCheck}
											value={key}
										/>
									</label>
								</td>
								<td>
									<button className="button is-danger" value={key} onClick={handleClick}>
										delete
									</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
};

export default TodoTable;
