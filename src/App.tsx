import React, { Suspense} from 'react'
import './App.css'

import Router from './navigation/Rounter'

const App = () => {
  return (
    <Suspense fallback={<h1>Loading...</h1>}>
      <Router />
    </Suspense>
  )
}

export default App
